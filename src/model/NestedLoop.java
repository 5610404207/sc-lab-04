package model;

public class NestedLoop {

		public String pattern1(int x) {
			String output = "";
			for (int i = 1; i<= x; i++){
				for (int j = 1; j <= x; j++)
					output += "*";
				output += "\n";
			}
			return output;
		}
		

		public String pattern2(int x) {
			String output = "";
			for (int i = 1; i<= x; i++){
				for (int j = 1; j <= x; j++)
					output += "*";
				output += "\n";
			}
			return output;
		}
		
		public String pattern3(int x) {
			String output = "";
			for (int i = 1; i<= x; i++){
				for (int j = 1; j <= i; j++)
					output += "*";
				output += "\n";
			}
			return output;
		}
		
		public String pattern4(int x) {
			String output = "";
			for (int i = 1; i<= x; i++){
				for (int j = 1; j <= x; j++)
					if (j%2 == 0)
						output += "*";
					else 
						output += "-";
				output += "\n";
			}
			return output;
		}
		
		public String pattern5(int x) {
			String output = "";
			for (int i = 1; i<= x; i++){
				for (int j = 1; j <= x; j++)
					if ((i+j)%2 == 0)
						output += "*";
					else 
						output += " ";
				output += "\n";
			}
			return output;
		}
		
		
}
