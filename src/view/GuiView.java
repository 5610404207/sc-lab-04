package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import controller.LoopController;

public class GuiView {

	public void setController(LoopController loopController)  {
	 	
	 	JFrame frame = new JFrame();
	 	frame.setVisible(true);
	 	frame.setLayout(new BorderLayout());
	  
	 	frame.setTitle("Lab04");
	 	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 	frame.pack();
	 	frame.setSize(500,250);
	 	frame.setLocation(10, 10);
	 	
	 	JPanel panel1 = new JPanel();
	 	panel1.setLayout(new BorderLayout());
	 	
	 	JPanel panel2 = new JPanel();
	 	panel2.setLayout(new GridLayout(1,1));
	 	
	 	String[] printList = { "Prints 3 rows of 4 asterisk each","Prints 4 rows of 3 asterisk each" ,"Prints 4 rows of length" , "Print asterisk in ever columns", "Print a checkboard"};
	 	String[] sizeList = {"1","2","3","4"};
	 	JComboBox<String> print = new JComboBox<String>(printList);
	 	JComboBox<String> print2 = new JComboBox<String>(sizeList);
	 	
	 		
	 	JTextArea textarea = new JTextArea ( );
	 	textarea.setText ( "Output" );
	 	textarea.setSize(300, 250);
	 	textarea.setLocation(250, 0);
	 	
	 	JButton b1 = new JButton("test");
	 	b1.addActionListener(new ActionListener() {
	 		 public void actionPerformed(ActionEvent e) {
	 			textarea.setText(loopController.select(print.getSelectedIndex()+1, print2.getSelectedIndex()+1));
	 		 }
	 		 });
 	
	 	panel2.add(print);
	 	panel2.add(print2);
	 	frame.add(b1,BorderLayout.SOUTH);
	 	frame.add(textarea,BorderLayout.CENTER);
	 	
	 	
	 	
	 	frame.add(panel2,BorderLayout.NORTH);
	}
}




