package controller;

import java.util.ArrayList;

import view.GuiView;
import model.NestedLoop;

public class LoopController {
		
		private NestedLoop model;
		private GuiView view;
		private ArrayList<String> nl; 

		public LoopController(NestedLoop loop, GuiView view) {
			this.model = loop;
			this.view = view;
			
			nl = new ArrayList<String>();

		 }
		
		 public String select(int x,int y){
			 nl.add(model.pattern1(y));
			 nl.add(model.pattern2(y));
			 nl.add(model.pattern3(y));
			 nl.add(model.pattern4(y));
			 nl.add(model.pattern5(y));
			 return nl.get(x);
		 }
		  
	}


