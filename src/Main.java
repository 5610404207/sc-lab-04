import controller.LoopController;
import view.GuiView;
import model.NestedLoop;


public class Main {

	public static void main(String[] args) {
		new Main();
		
	}
	
	public Main() {
		gui = new GuiView();
		loop = new NestedLoop();
		LoopController lc = new LoopController(loop, gui);
		gui.setController(lc);
	}
	GuiView gui;
	NestedLoop loop;
	
}
